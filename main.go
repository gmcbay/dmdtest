package main

import (
	"bitbucket.org/gmcbay/dmd"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"io/ioutil"
	"log"
	"os"
	"time"
)

func main() {
	log.Printf("dmdtest...\n")

	var err error
	var dev *dmd.Device

	if err = dmd.Initialize(); err != nil {
		log.Printf("Error on Initialize: %v\n", err)
		os.Exit(1)
	}

	if devCount := dmd.NumDev(); devCount != 1 {
		log.Printf("Unexpected number of DMD devices detected (%v founded; expected 1)\n", devCount)
		os.Exit(1)
	}

	if dev, err = dmd.ConnectDevice(0); err != nil {
		log.Printf("Error on ConnectDevice: %v\n", err)
		os.Exit(1)
	}

	dev.ClearFifos()

	imgSize := 1920 * 1080

	dev.Clear(dmd.AllBlocks, true)

	imageList := make([][]byte, 0)

	imgs, _ := ioutil.ReadDir("./img")

	for _, img := range imgs {
		imgBytes := make([]byte, (imgSize)/8)

		f, _ := os.Open("./img/" + img.Name())
		defer f.Close()
		imgData, _ := png.Decode(f)

		b := imgData.Bounds()
		rgbaImg := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
		draw.Draw(rgbaImg, rgbaImg.Bounds(), imgData, b.Min, draw.Src)

		xMax := rgbaImg.Bounds().Dx()
		yMax := rgbaImg.Bounds().Dy()

		for y := 0; y < yMax; y++ {
			for x := 0; x < xMax; x++ {
				r, g, b, _ := rgbaImg.At(x, y).RGBA()

				idx := 1920*y + x

				posByte := uint(idx / 8)
				posBit := uint(idx % 8)
				current := ((0xFF7F >> posBit) & int(imgBytes[posByte])) & 0x00FF

				if r+g+b < 384 {
					imgBytes[posByte] = ((0 << (8 - (posBit + 1))) | byte(current))
				} else {
					imgBytes[posByte] = ((1 << (8 - (posBit + 1))) | byte(current))
				}
			}
		}

		imageList = append(imageList, imgBytes)
	}

	iMax := len(imageList)

	frames := 0
	startTime := time.Now()

	for j := 0; j < 50; j++ {
		for i := 0; i < iMax; i++ {
			dev.LoadFrame(imageList[i], dmd.AllBlocks, true)
			frames++
		}
	}

	elapsed := time.Since(startTime)

	fmt.Printf("%v frames in %v, (%v fps)\n", frames, elapsed, float64(frames)/elapsed.Seconds())

}
